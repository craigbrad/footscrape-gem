# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'footscrape/version'

Gem::Specification.new do |spec|
  spec.name          = "footscrape"
  spec.version       = Footscrape::VERSION
  spec.authors       = ["Craig Bradley"]
  spec.email         = ["craigpbradley93@gmail.com"]
  spec.summary       = %q{This gem is used to scrape from liveonsat and store to db}
  spec.description   = %q{This gem is used to get data from live on sat. The data will be parsed and stored in a db for later use with an ios app. The data scraped will include the channel listing for each football match.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "~> 3.0.0"
  spec.add_development_dependency "nokogiri", "~> 1.6"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "vcr", "~> 2.9.0"
  spec.add_development_dependency "rmagick", "~>2.13.2"
  spec.add_development_dependency "rtesseract", "~> 1.2.0"
end
