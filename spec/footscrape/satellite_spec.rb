require 'spec_helper'

describe Footscrape::Satellite do

  let(:satellite) { Footscrape::Satellite.new(data, 0) }
  let(:data) { double('Data') }
  let(:rest_cols) {
    [
      double('RestCol1', text: 'satellite'),
      double('RestCol2', text: 'Thor 5'),
      double('RestCol3', text: '33000 - 3/4'),
    ]
  }
  let(:pos_cols) { [double('PosCol', text: '19.2e')] }
  let(:freq_cols) { [double('FreqCol', text: '10.774 V')] }
  let(:enc_cols) { [double('EncCol', text: 'VideoGuard')] }

  before do
    allow(data).to receive(:css).with('td.rest_col').and_return(rest_cols)
    allow(data).to receive(:css).with('td.pos_col').and_return(pos_cols)
    allow(data).to receive(:css).with('td.freq_col').and_return(freq_cols)
    allow(data).to receive(:css).with('td.enc_not_live', 'td.enc_live').
      and_return(enc_cols)
  end

  let(:expected_hash) {
    {
      position: '19.2e',
      name: 'Thor 5',
      frequency: '10.774 V',
      symbol: '33000 - 3/4',
      encryption: 'VideoGuard',
    }
  }

  describe '#to_hash' do
    it 'returns satellite data' do
      expect(satellite.to_hash).to eq(expected_hash)
    end
  end
end
