require "spec_helper"

describe Footscrape::Game do

  let(:game) { Footscrape::Game.new(uri, data) }
  let(:uri) { 'http://liveonsat.com' }
  let(:data) { double('Data') }
  let(:image) { 
    double(
      'Image',
      home_team: 'Manchester City',
      away_team: 'Manchester United',
    ) 
  }
  let(:channels) { [channel] }
  let(:channel) {
    double(
        'channel',
        date: '25/06/15',
        attributes: ''
      )
  }
  let(:channel_hash) {
    {
      name: 'Sky Sports 1',
      satellites: []
    }
  }
  let(:time) { double('Time', text: 'ST: 15:00') }
  
  before do
    allow(game).to receive(:image).and_return(image)
    allow(data).to receive(:css).with('a').and_return(channels)
    allow(data).to receive(:css).with('div.fLeft_time_live').and_return(time)
    allow(Footscrape::Channel).to receive(:new).and_return(channel)
    allow(channel).to receive(:to_hash).and_return(channel_hash)
  end

  describe '#to_hash' do
    let(:expected_hash) {
      {
        home_team: 'Manchester City',
        away_team: 'Manchester United',
        date: '25/06/15',
        time: '15:00',
        channels: [
          {
            name: 'Sky Sports 1', 
            satellites: []
          }
        ]
      }
    }

    it 'returns a hash with the game and channel data' do
      expect(game.to_hash).to eq(expected_hash)
    end
  end


end
