require 'spec_helper'

describe Footscrape::Satellites do

  let(:satellites) { Footscrape::Satellites.new(data) }
  let(:data) { double('Data') }
  let(:hash) {
      {
        name: 'Amos 2',
        position: '4.0W',
        frequency: '10.824 V',
        encryption: 'VideoGuard',
        symbol: '27500 - 3/4',
      }
    }

  describe '#from_uri' do
    before do
      allow(data).to receive(:css).with('td.pos_col').and_return([double])
      allow(Footscrape::Satellite).to receive(:to_hash).and_return(hash)
    end

    let(:expected_hash) {
      {
        name: 'Amos 2',
        position: '4.0W',
        frequency: '10.824 V',
        encryption: 'VideoGuard',
        symbol: '27500 - 3/4',
      }
    }

    it 'returns an array of satellites' do
      expect(satellites.from_uri).to eq([expected_hash])
    end
  end

end
