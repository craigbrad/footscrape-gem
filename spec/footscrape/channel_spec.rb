require "spec_helper"

describe Footscrape::Channel do

  let(:channel) { Footscrape::Channel.new(document) }
  let(:document) {
    double(
      'Document',
      text: 'Sky Sports 1',
      attributes: attributes,
    )
  }
  let(:attributes) { double('Attributes') }
  let(:onmouseover) {
    double(
      'onmouseover',
      value: 'Fox Sports 2 HD (ned)  – 23/05/15 - 17:30'
    )
  }

  before do
    allow(attributes).to receive(:[]).with('onmouseover').and_return(onmouseover)
  end

  describe '#to_hash' do
    let(:expected_hash) {
      {
        name: 'Sky Sports 1',
        satellites: [],
      }
    }

    it 'returns a hash with the channel name and satellites' do
     expect(channel.to_hash).to eq(expected_hash) 
    end
  end


  describe '#date' do
    it 'returns the airing date' do
      expect(channel.date).to eq('23/05/15')
    end
  end

end
