require "spec_helper"

vcr_options = {
  :cassette_name => 'live_on_sat/champions_league'
}

describe Footscrape, :vcr => vcr_options do
  let(:uri) {'http://liveonsat.com/los_soc_int_UEFA_CL.php'}

  describe '#scrape_games' do
    let(:games) { Footscrape.scrape_games(uri) }

    context 'successfully scrape the site' do
      it 'returns expected data' do
        expect(games[0].fetch(:home_team)).to eq('Juventus')
        expect(games[0].fetch(:away_team)).to eq('Barcelona')
        expect(games[0].fetch(:date)).to eq('06/06/15')
        expect(games[0].fetch(:time)).to eq('19:45')
        expect(games[0].fetch(:channels).count).to eq(63)
      end
    end
  end

end
