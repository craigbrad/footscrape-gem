# Footscrape

This is a ruby gem that uses Nokogiri to scrape data from liveonsat.com

## Installation

Add this line to your application's Gemfile:

    gem 'footscrape'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install footscrape

## Usage

To use this gem follow the instructions above to install and then:

To scrape liveonsat.com use the method
```ruby
games = Footscrape.scrape_games(uri)
```
this method will grab all of the premiership games and the channels they are on.

You can access the channels and the channel satellites as follows

```ruby
channels = games.channels
satellites = games.channels[0].satellites
```

## Contributing

1. Fork it ( https://github.com/[my-github-username]/footscrape/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request