# encoding: utf-8
require 'open-uri'
require 'nokogiri'
require 'footscrape/game'

class Footscrape

  attr_reader :uri
  protected :uri

  def initialize(uri)
    @uri = uri
  end

  def self.scrape_games(uri)
    new(uri).scrape_games
  end

  def scrape_games
    divs.map { |node| Game.to_hash(uri, node) }
  end

  private

  def divs
    webpage.css('div.blockfix')
  end

  def webpage
    Nokogiri::HTML(open(uri).read, nil, 'utf-8')
  end

end
