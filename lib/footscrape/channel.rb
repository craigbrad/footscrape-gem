require 'footscrape/satellites'

class Footscrape
  class Channel

    attr_reader :document
    protected :document

    def initialize(document)
      @document = document
    end

    def to_hash
      {
        name: name,
        satellites: satellites,
      }
    end

    def date
      @date ||= data.text[/..\/..\/../]
    end

    private

    def name
      @name ||= document.text
    end

    def data
      @data ||= Nokogiri::HTML(document.attributes["onmouseover"].value)
    end

    def satellites
      Satellites.from_uri(data)
    end

  end
end
