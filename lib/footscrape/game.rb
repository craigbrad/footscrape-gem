require 'footscrape/channel'
require 'footscrape/game_image'

class Footscrape
  class Game

    def initialize(uri, data)
      @uri = uri
      @data = data
    end

    def self.to_hash(uri, data)
      new(uri, data).to_hash
    end

    def to_hash
      {
        home_team: home_team,
        away_team: away_team,
        date: date,
        time: time,
        channels: channels.map(&:to_hash),
      }
    end

    private

    def home_team
      image.home_team
    end

    def away_team
      image.away_team
    end

    def date
      channels[0].date
    end

    def time
      @data.css('div.fLeft_time_live').text.strip[4..-1]
    end

    def channels
      channels_data.map { |channel| Channel.new(channel) }
    end

    def channels_data
      @channels_data ||= @data.css('a')
    end

    def image
      @image ||= GameImage.new(image_uri)
    end

    def image_src
      @image_src ||= @data.css('div.fLeft').css('img').attr('src').value
    end

    def image_uri
      @image_uri ||= URI.join( @uri, image_src ).to_s
    end

  end
end
