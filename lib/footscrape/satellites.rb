require 'footscrape/satellite'

class Footscrape
  class Satellites

    attr_reader :data
    protected :data

    def initialize(data)
      @data = data
    end

    def self.from_uri(data)
      new(data).from_uri
    end

    def from_uri
      satellites
    end

    private

    def positions
      @positions ||= data.css('td.pos_col')
    end

    def satellites
      (0..positions.count - 1).map do |index|
        Satellite.to_hash(data, index)
      end
    end

  end
end
