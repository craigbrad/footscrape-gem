class Footscrape
  class Satellite

    attr_reader :data, :index
    protected :data, :index

    def initialize(data, index)
      @data, @index = data, index
    end

    def self.to_hash(data, index)
      new(data, index).to_hash
    end

    def to_hash
      {
        name: names[index*2].text,
        position: positions[index].text,
        frequency: frequencies[index].text,
        encryption: encryptions[index].text,
        symbol: symbols[index*2+1].text,
      }
    end

    private

    def positions
      data.css('td.pos_col')
    end

    def names_and_symbols
      @ns ||= rest_cols.reject do |item|
        item.text == 'satellite'  ||
        item.text == 'symbol'     ||
        item.text == 'encryption'
      end
    end
    alias_method :names, :names_and_symbols
    alias_method :symbols, :names_and_symbols

    def rest_cols
      data.css('td.rest_col')
    end

    def frequencies
      data.css('td.freq_col')
    end

    def encryptions
      data.css('td.enc_not_live', 'td.enc_live')
    end

  end
end
