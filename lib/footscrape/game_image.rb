require 'rmagick'
require 'rtesseract'

class Footscrape
  class GameImage
    include Magick

    def initialize(uri)
      @uri = uri
    end

    def create_image
      open('image.png', 'wb') do |file|
        file << open(@uri).read
      end
    end

    def get_image
      @image ||= ImageList.new('image.png')
    end

    def resize
      get_image.resize!(1080, 60)
    end

    def image
      create_image
      resize.write('image.png')
    end

    def text
      image
      @text ||= RTesseract.new('image.png').to_s.strip
    end

    def teams
      @teams ||= text.split(' v ')
    end

    def home_team
      @home_team ||= teams[0]
    end

    def away_team
      @away_team ||= teams[1]
    end

  end
end
